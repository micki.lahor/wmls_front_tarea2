import React from 'react'
import { Box } from '@mui/material'
import Navbar from '../components/navbar/Navbar'
import Banner from '../components/banner/Banner'
import Services from '../components/services/Services'
import Review from '../components/review/Review'
import Contact from '../components/contact/Contact'

function MainPage() {
  return (
    <Box>
      {<Navbar />}
      {<Banner />}
      {<Services />}
      {<Review />}      
      {<Contact />}
    </Box>
  )
}

export default MainPage
