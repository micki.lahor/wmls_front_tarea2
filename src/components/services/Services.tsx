import React from 'react'
import './Services.css'
import { Box, Typography } from '@mui/material'
import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward'

export default function Services() {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="static"
      style={{ paddingTop: '10px', margin: '0 150px' }}
    >
      <div className="container-services">
        <Typography
          variant="h6"
          fontWeight={'bold'}
          sx={{ mb: 4 }}
          style={{ paddingTop: '40px' }}
        >
          Nuestros Servicios
        </Typography>
        <div className="services-grid">
          <div className="service-card">
            <img src="/images/image 4.png" alt="Odontología Cosmética" />
            <Typography variant="h6" fontWeight={'bold'} sx={{ mb: 4 }}>
              Odontologia Cosmetica
            </Typography>
            <Typography
              fontWeight={'bold'}
              sx={{ mb: 4, color: '#006a65', textAlign: 'center' }}
            >
              Mas Informacion
              <ArrowOutwardIcon sx={{ color: '006a65' }} />
            </Typography>
          </div>
          <div className="service-card">
            <img src="/images/image 3.png" alt="Odontología Restaurativa" />
            <Typography variant="h6" fontWeight={'bold'} sx={{ mb: 4 }}>
              Odontologia Restaurativa
            </Typography>
            <Typography
              fontWeight={'bold'}
              sx={{ mb: 4, color: '#006a65', textAlign: 'center' }}
            >
              Mas Informacion
              <ArrowOutwardIcon sx={{ color: '006a65' }} />
            </Typography>
          </div>
          <div className="service-card">
            <img src="/images/image 6.png" alt="Odontopediatría" />
            <Typography variant="h6" fontWeight={'bold'} sx={{ mb: 4 }}>
              Odontopediatría
            </Typography>
            <Typography
              fontWeight={'bold'}
              sx={{ mb: 4, color: '#006a65', textAlign: 'center' }}
            >
              Mas Informacion
              <ArrowOutwardIcon sx={{ color: '006a65' }} />
            </Typography>
          </div>
          <div className="service-card">
            <img src="/images/image 5.png" alt="Ortodoncia" />
            <Typography variant="h6" fontWeight={'bold'} sx={{ mb: 4 }}>
              Ortodoncia
            </Typography>
            <Typography
              fontWeight={'bold'}
              sx={{ mb: 4, color: '#006a65', textAlign: 'center' }}
            >
              Mas Informacion
              <ArrowOutwardIcon sx={{ color: '006a65' }} />
            </Typography>
          </div>
        </div>
      </div>
    </Box>
  )
}
