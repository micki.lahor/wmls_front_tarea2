import React from 'react'
import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'
import DarkModeOutlinedIcon from '@mui/icons-material/DarkModeOutlined'

export default function Navbar() {
  return (
    <AppBar position="static">
      <Toolbar>
        <img src="/images/Group 19.png" alt="Logo" />

        <Box sx={{ flexGrow: 1 }} />
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <Button color="primary" sx={{ color: 'black' }}>
            Nosotros
          </Button>
          <Button color="inherit" sx={{ color: 'black' }}>
            Servicios
          </Button>
          <Button color="inherit" sx={{ color: 'black' }}>
            Nuevos pacientes
          </Button>
          <Button color="inherit" sx={{ color: 'black' }}>
            Contacto
          </Button>
        </Box>
        <Box sx={{ flexGrow: 1 }} />
        <IconButton size="large" aria-label="search" color="primary">
          <SearchIcon htmlColor="#006a65" fontSize="large" />
        </IconButton>
        <IconButton size="large" aria-label="search" color="primary">
          <DarkModeOutlinedIcon htmlColor="#006a65" fontSize="large" />
        </IconButton>
        <Button
          variant="contained"
          color="primary"
          style={{ borderRadius: '10px', fontWeight: 'bold' }}
        >
          Reservar
        </Button>
      </Toolbar>
    </AppBar>
  )
}
