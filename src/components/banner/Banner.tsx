import React from 'react'
import { Box, Button, Typography } from '@mui/material'

export default function Banner() {
  return (
    <Box
      display="flex"
      flexDirection="row"
      p={1}
      m={1}
      bgcolor="background.paper"
      justifyContent="center"
    >
      <Box p={1}>
        <Typography style={{ paddingTop: '30px', color: 'primary' }}>
          Clinica odontologica especialista
          <Typography
            variant="h4"
            style={{ paddingTop: '30px', fontSize: '2.5em', color: 'primary' }}
          >
            Atencion para todas las edades
          </Typography>
          <Button
            variant="contained"
            color="primary"
            style={{ borderRadius: '10px' }}
          >
            Reservar en linea
          </Button>
          <Typography variant="h6" style={{ paddingTop: '20px' }}>
            o llama al 800-10-01-02
          </Typography>
        </Typography>
      </Box>
      <Box p={1}>
        <img src="/images/image 2.png" />
      </Box>
    </Box>
  )
}
