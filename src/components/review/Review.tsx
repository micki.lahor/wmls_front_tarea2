import React from 'react'
import { Typography, Box } from '@mui/material'

export default function Review() {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="static"
      style={{ paddingTop: '10px', margin: '0 150px' }}
    >
      <Box style={{ paddingLeft: '150px' }}>
        <Typography
          variant="h5"
          fontWeight={'bold'}
          sx={{ mb: 4 }}
          style={{ paddingTop: '40px' }}
        >
          Opiniones de nuestros pacientes
        </Typography>
      </Box>
      <Box display="flex" justifyContent="space-around" width="100%">
        <Box
          bgcolor="white"
          border="1px solid black"
          display="flex"
          flexDirection="column"
          alignItems="left"
          p={1}
          m={1}
          flexGrow={1}
          style={{ paddingTop: '50px', borderRadius: '20px' }}
        >
          <Typography sx={{ mb: 4 }}>
            &quot;Excelente servicio en Brillo Dental, Trato amable,
            precedimientos indolores. ¡Mi sonrisa nunca ha estado mejor!&quot;
          </Typography>
          <Typography fontWeight={'bold'} sx={{ mb: 4, color: '006a65' }}>
            Lucia P.
          </Typography>
        </Box>
        <Box
          bgcolor="white"
          border="1px solid black"
          display="flex"
          flexDirection="column"
          alignItems="left"
          p={1}
          m={1}
          flexGrow={1}
          style={{ paddingTop: '50px', borderRadius: '20px' }}
        >
          <Typography sx={{ mb: 4 }}>
            &quot;En Magia Dental me sentí como en casa, Profesionales
            excepcionales y resultados sorprendentes. ¡Muy recomendado!&quot;
          </Typography>
          <Typography fontWeight={'bold'} sx={{ mb: 4, color: '006a65' }}>
            Jorge E.
          </Typography>
        </Box>
        <Box
          bgcolor="white"
          border="1px solid black"
          display="flex"
          flexDirection="column"
          alignItems="left"
          p={1}
          m={1}
          flexGrow={1}
          style={{ paddingTop: '50px', borderRadius: '20px' }}
        >
          <Typography sx={{ mb: 4 }}>
            &quot;Magia Dental transformo mi sonrisa con eficiencia y cuidado.
            ¡El mejor dentista al que he ido!&quot;
          </Typography>
          <Typography fontWeight={'bold'} sx={{ mb: 4, color: '006a65' }}>
            Sofia M.
          </Typography>
        </Box>
      </Box>
    </Box>
  )
}
